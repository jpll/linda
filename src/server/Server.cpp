/*
 * Server.cpp
 *
 *  Created on: 20 maj 2014
 *      Author: jakub
 */

#include "Server.hpp"

Server::Server(const Config config)
:
		config(config), running(false)
{
	BOOST_LOG_SEV(logger, info)<< "Tuple space name: " << this->config.tupleSpaceName;
	BOOST_LOG_SEV(logger, debug)<< "Main path: "<< this->config.lindaDir;

	if(this->config.tupleSpaceDir.length() > 69)
	{
		throw std::logic_error("Tuple space name too long");
	}

	const char* cFifoPath = this->config.fifoPath.c_str();
	// Tylko serwer czyta z fifo, każdy inny może dodawać
	if (mkfifo(cFifoPath, S_IRUSR | S_IWUSR | S_IWGRP | S_IWOTH) < 0)
	{
		throw std::runtime_error("Cannot create server fifo. Fifo path: " + config.fifoPath);
	}

	if ((fifoFD = open(cFifoPath, O_RDWR)) < 0)
	{
		throw std::runtime_error("Cannot open server fifo. Fifo path: " + config.fifoPath);
	}
	BOOST_LOG_SEV(logger, debug)<< "Server fifo: "<< config.fifoPath;
}

Server::~Server()
{

}

void Server::readFifo()
{
	const int bufferSize = 4096;
	char buffer[bufferSize];

	BOOST_LOG_SEV(logger, debug)<< "Start fifo reading.";

	int n;
	std::string bufString;
	size_t barPosition;  //pozycja symbolu '|'

	while ((n = read(fifoFD, buffer, bufferSize - 1)) > 0)
	{
		buffer[n] = '\0';
		BOOST_LOG_SEV(logger, debug)<< "FIFO input: " << buffer;

		bufString += std::string(buffer);

		if(bufString.length() > bufferSize)
		{
			BOOST_LOG_SEV(logger, warning)<< "Too long command: " << bufString;
			bufString = "";
		}

		std::string commandString;
		std::shared_ptr<Command> command;
		while ((barPosition = bufString.find('|')) != std::string::npos)
		{
			commandString = bufString.substr(0, barPosition);
			bufString = bufString.substr(barPosition + 1);
			try
			{
				command = CommandFactory::createCommand(commandString);
				queue.push(command);
				BOOST_LOG_SEV(logger, debug)<< "Command added to Queue: " << command->serialize();
			}
			catch (...)
			{
				BOOST_LOG_SEV(logger, debug)<< "Command cannot be created from string: " << commandString;
			}

		}
	}
	BOOST_LOG_SEV(logger, debug)<< "Reading fifo stopped.";
	// Nie powinno tu dojść, jeżeli doszło to znaczy, że coś jest nie tak
	// podnosimy sygnał SIGUSR1 na znak, że wątek readFifo się zakończył.
	raise(SIGUSR1);
}

void Server::executeCommands()
{
	try
	{
		// Lokalna kolejka poleceń do wykonania.
		// Zawiera nowe polecenia, jak i polecenia, których wykonanie
		// się nie powiodło, ale mają timeout.
		std::queue<std::shared_ptr<Command>> commandsToDo;
		std::chrono::milliseconds max(MAX_TIMEOUT);
		std::chrono::milliseconds timeoutWait(max);
		BOOST_LOG_SEV(logger, debug)<< "Timeout: " << timeoutWait.count();
		while (true)
		{
			auto isSuccess = false;
			auto command = queue.offer(isSuccess, timeoutWait);
			if (isSuccess && command.get() != nullptr)
			{
				if(dynamic_cast<OutputCommand*>(command.get()) != nullptr)
				{
					command->execute(tupleSpace);
				}
				else
				{
					commandsToDo.push(command);
				}
			}
			auto commandsNo = commandsToDo.size();
			// Wykonujemy wszystkie operacje
			// z lokalnej kolejki operacji do wykonania.
			timeoutWait = std::chrono::milliseconds(max);
			for (unsigned int i = 0; i < commandsNo; i++)
			{
				command = commandsToDo.front();
				commandsToDo.pop();

				BOOST_LOG_SEV(logger, debug)<< "Executing: " << command->serialize();

				// Jeżeli nie wykonała się to znaczy, że
				// trzeba spróbować później.
				// Operacja Output powinna się ZAWSZE udać.
				if (!command->execute(tupleSpace) && command->getTimeToDeadline().count() > 0)
				{
					BOOST_LOG_SEV(logger, debug)<< "Execution failed.";
					BOOST_LOG_SEV(logger, debug)<< "Will try later.";
					timeoutWait = std::min(timeoutWait, command->getTimeToDeadline());
					commandsToDo.push(command);
				}
				else
				{
					BOOST_LOG_SEV(logger, debug)<< "Execution succeeded or deadline passed.";
				}
			}
			BOOST_LOG_SEV(logger, debug)<< "Timeout: " << timeoutWait.count();
		}
	} catch (const std::exception& e)
	{
		BOOST_LOG_SEV(logger, error)<< "Error occurred while executing commands.";
		BOOST_LOG_SEV(logger, debug) << e.what();
	}
	// Nie powinno tu dojść, jeżeli doszło to znaczy, że coś jest nie tak
	// podnosimy sygnał SIGUSR1 na znak, że wątek executeCommands się zakończył.
	raise(SIGUSR1);
}

void Server::run()
{
	if (running)
	{
		throw std::logic_error("Server is already running.");
	}

	//UWAGA: executeCommandsThread zakomentowane dla wygody sprawdzenia działania.

	std::thread readFifoThread(&Server::readFifo, this);
	std::thread executeCommandsThread(&Server::executeCommands, this);

	running = true;

	readFifoThread.join();
	executeCommandsThread.join();

	// Nie powinno tutaj dojść.
	// Tj. wcześniej któryś z wątków powinien podnieść sygnał o tym, że się zakończył.
	running = false;
	throw std::runtime_error("Error occurred while running server");
}

bool Server::isRunning() const
{
	return running;
}

