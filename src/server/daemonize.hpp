/*
 * daemon.hpp
 *
 *  Created on: 16 maj 2014
 *      Author: jakub
 */

#pragma once

#include <csignal>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

#include <stdexcept>
#include <fstream>

#include "../common/defines.hpp"

namespace daemonize
{
	class daemon_failed_to_stop_exception: public std::exception
	{

	};

	class daemon_not_running_exception: public std::exception
	{

	};

	/**
	 * Zapisuje pid procesu do pliku.
	 * @param fileName - nazwa pliku (ze ścieżką), do którego zostanie zapisane pid_t procesu.
	 */
	void save_pid(std::string fileName);

	/**
	 * Kasuje plik pid.
	 */
	void delete_pid_file();

	/**
	 * Zmienia proces w daemona, patrz: http://pl.wikipedia.org/wiki/Demon_(informatyka)
	 */
	void turn_into_daemon();

	/**
	 * Zatrzymuje daemona.
	 * @param fileName - plik zawieracjący pid daemona.
	 */
	void stop_daemon(std::string fileName);
}
