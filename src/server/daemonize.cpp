/*
 * daemon.cpp
 *
 *  Created on: 16 maj 2014
 *      Author: jakub
 */

#include "daemonize.hpp"

namespace daemonize
{
	namespace
	{
		std::string pidFileName;
	}

	void save_pid(std::string fileName)
	{
		std::ofstream pidFile;
		pidFile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
		try
		{
			pidFile.open(fileName.c_str(), std::ios_base::out | std::ios_base::trunc);
			pid_t pid = getpid();
			pidFile << pid << '\n';
			// Zachowanie nazwy pliku z pid daemona
			pidFile.close();
			pidFileName = fileName;
			// To powoduje, że przy exit() zostanie
			// wywołana funkcja delete_pid_file()
			std::atexit(delete_pid_file);
		}
		catch (...)
		{
			std::rethrow_exception(std::current_exception());
		}

	}

	void stop_daemon(std::string fileName)
	{
		std::ifstream pidFile;
		pidFile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
		try
		{
			pidFile.open(fileName.c_str(), std::ios_base::in);
			pid_t pid;
			pidFile >> pid;
			if (kill(pid, SIGINT) != 0)
			{
				throw daemon_failed_to_stop_exception();
			}
			pidFile.close();
		}
		catch (const std::ofstream::failure& e)
		{
			throw daemon_not_running_exception();
		}
		catch (...)
		{
			std::rethrow_exception(std::current_exception());
		}
	}

	void delete_pid_file()
	{
		remove(pidFileName.c_str());
	}

	/**
	 * Zmienia proces w daemona, patrz: http://pl.wikipedia.org/wiki/Demon_(informatyka)
	 */
	void turn_into_daemon()
	{
		pid_t pid;

		/* Fork off the parent process */
		pid = fork();

		/* An error occurred */
		if (pid < 0)
			throw std::runtime_error("first fork() error");

		/* Success: Let the parent terminate */
		if (pid > 0)
			exit(EXIT_SUCCESS);

		/* On success: The child process becomes session leader */
		if (setsid() < 0)
			throw std::runtime_error("setsid() error");

		/* Catch, ignore and handle signals */
		//TODO: Implement a working signal handler */
		signal(SIGCHLD, SIG_IGN);
		signal(SIGHUP, SIG_IGN);

		/* Fork off for the second time*/
		pid = fork();

		/* An error occurred */
		if (pid < 0)
			throw std::runtime_error("sec fork() error");

		/* Success: Let the parent terminate */
		if (pid > 0)
			exit(EXIT_SUCCESS);

		/* Set new file permissions */
		umask(0);

		/* Change the working directory to the root directory */
		/* or another appropriated directory */
		chdir("/");

		/* Close stdin, stdout, and stderr */
		(void) close(STDIN_FILENO);
		(void) close(STDOUT_FILENO);
		(void) close(STDERR_FILENO);

		/* Close all open file descriptors */
		int x;
		for (x = sysconf(_SC_OPEN_MAX); x > 0; x--)
		{
			close(x);
		}

		/* Redirect stdin, stdout, and stderr to /dev/NULL */
		if (open("/dev/null", O_RDWR) != 0)
		{
			throw std::runtime_error("error opening /dev/null");
		}

		(void) dup(0);
		(void) dup(0);
	}
}
