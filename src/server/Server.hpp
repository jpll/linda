/*
 * Server.h
 *
 *  Created on: 20 maj 2014
 *      Author: jakub
 */

#pragma once

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <csignal>

#include <thread>
#include <queue>
#include <list>
#include <algorithm>
#include <chrono>

#include "../common/common.hpp"

#include "../common/BlockingQueue.hpp"
#include "../common/CommandFactory.hpp"

class Server
{
	public:

		struct Config
		{
			std::string lindaDir;
			std::string tupleSpaceDir;
			std::string tupleSpaceName;
			std::string fifoPath;
		} typedef Config;

	private:

		/** Logger. */
		Logger logger;

		/** Konfiguracja serwera: główna ścieżka, nazawa przestrzeni krotek, nazwa fifo. */
		const Config config;
		/** Fifo file desc. */
		int fifoFD;
		/** Przestrzeń krotek. */
		std::list<Tuple> tupleSpace;
		/** Kolejka poleceń. */
		BlockingQueue<std::shared_ptr<Command>> queue;
		/** Prawda jeżeli run() zostało wywołane, fałsz wpp. */
		bool running;

	public:
		Server(const Config);
		~Server();
		void run();
		bool isRunning() const;

	private:

		void readFifo();
		void executeCommands();
};

