#include <iostream>
#include <boost/program_options.hpp>

#include <csignal>

#include "../common/defines.hpp"
#include "../common/common.hpp"

#include "daemonize.hpp"
#include "Server.hpp"

namespace
{
	Server::Config serverConfig;
	Logger logger;
}

void kill_handler(int signum)
{
	BOOST_LOG_SEV(logger, info)<< "Caught signal: " << signum;
	BOOST_LOG_SEV(logger, info) << "Application will now exit.";
	exit(signum);
}

inline void install_signal_handlers()
{
	signal(SIGPIPE, kill_handler);
	signal(SIGINT, kill_handler);
	signal(SIGSEGV, kill_handler);
	signal(SIGTERM, kill_handler);
	signal(SIGABRT, kill_handler);
	// Odpowiadają za sygnały od klasy Server
	signal(SIGUSR1, kill_handler);
	signal(SIGUSR2, kill_handler);
}

int stop_daemon(std::string pidFileName)
{
	try
	{
		daemonize::stop_daemon(pidFileName);
		BOOST_LOG_SEV(logger, info)<< "Daemon stopped.";
		return EXIT_SUCCESS;
	}
	catch (const daemonize::daemon_failed_to_stop_exception& e)
	{
		BOOST_LOG_SEV(logger, warning)<< "Daemon not found or cannot be killed.";
		return EXIT_FAILURE;
	}
	catch (const daemonize::daemon_not_running_exception& e)
	{
		BOOST_LOG_SEV(logger, warning)<< "Daemon is not running.";
		return EXIT_FAILURE;
	}
	catch (const std::exception& e)
	{
		BOOST_LOG_SEV(logger, error)<< "Error occurred while stoping daemon.";
		common::print_exception(logger, e);
		return EXIT_FAILURE;
	}
}

void delete_server_fifo()
{
	if (unlink(serverConfig.fifoPath.c_str()) < 0)
	{
		BOOST_LOG_SEV(logger, error)<< "Cannot delete a fifo file. Fifo path: " + serverConfig.fifoPath;
	}
	BOOST_LOG_SEV(logger, debug)<< "Fifo file deleted.";
}

int main(int argc, char **argv)
{
	namespace cm = common;
	namespace po = boost::program_options;

	try
	{
		install_signal_handlers();
		cm::log::init();

		po::options_description general("General options");
		general.add_options()
		("help,h", "Produce help message.")
		("tuple-space,t", po::value<std::string>()->required()->value_name("space"), "Tuple space name.")
		("daemon,d", po::bool_switch()->default_value(false), "Start process as daemon.")
		("stop-daemon,c", po::bool_switch()->default_value(false), "Stop daemon.")
				;

		po::options_description special("Special options");
		special.add_options()
		("silent,S", po::bool_switch()->default_value(false), "Silent mode.")
		("debug,D", po::bool_switch()->default_value(false), "Print debug logs.")
				;

		po::options_description all("Allowed options");
		all.add(general).add(special);

		po::positional_options_description p;
		p.add("tuple-space", -1);

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				options(all).positional(p).run(), vm);

		if (vm.count("help"))
		{
			std::string usage(
					"Usage:"
					"\t server tuple-space-name [--daemon] [--debug | --silent]\n"
					"\t server --stop-daemon tuple-space-name\n"
					);
			std::cout << usage << "\n";
			std::cout << all << "\n";
			return EXIT_SUCCESS;
		}

		// NOTIFY - dopiero tutaj sprawdzana program rzuca wyjątki
		// jeżeli coś jest nie tak z opcjami uruchomienia.
		po::notify(vm);

		if (vm["debug"].as<bool>())
		{
			cm::log::set_severity_level(debug);
		}

		if (vm["silent"].as<bool>())
		{
			// SILENT LOGS
			cm::log::silent();
		}

		if (vm.count("tuple-space"))
		{
			try
			{
				std::string tupleSpaceName = vm["tuple-space"].as<std::string>();
				serverConfig.lindaDir = std::string(LINDA_DIR);
				serverConfig.tupleSpaceDir = serverConfig.lindaDir + tupleSpaceName + "/";
				serverConfig.tupleSpaceName = tupleSpaceName;
				serverConfig.fifoPath = serverConfig.tupleSpaceDir + serverConfig.tupleSpaceName;

				std::string pidFileName = serverConfig.fifoPath + ".pid";

				if (vm["stop-daemon"].as<bool>())
				{
					return stop_daemon(pidFileName);
				}

				// Sprawdza istnienie fifo serwera
				if(access(serverConfig.fifoPath.c_str(), F_OK) == 0)
				{
					BOOST_LOG_SEV(logger, error) << "Server with this tuple space name already exists.";
					return EXIT_FAILURE;
				}

				// Utwórz katalog programu, jeśli nie istnieje.
				if (access(serverConfig.lindaDir.c_str(), F_OK))
				{
					if (mkdir(serverConfig.lindaDir.c_str(), 0777))
					{
						throw std::runtime_error(
							"Cannot create linda directory: " + serverConfig.lindaDir);
					}
				}
				// Sprawdź czy katalog programu jest katalogiem i czy są odpowiednie prawa.
				if (!common::files::isDir(serverConfig.lindaDir)
					|| access(serverConfig.lindaDir.c_str(), W_OK | R_OK))
				{
					throw std::runtime_error(
						"Cannot access linda directory: " + serverConfig.lindaDir);
				}

				// Utwórz katalog do obsługi podanej przestrzeni krotek, jeśli nie istnieje.
				if (access(serverConfig.tupleSpaceDir.c_str(), F_OK))
				{
					if (mkdir(serverConfig.tupleSpaceDir.c_str(), 0777))
					{
						throw std::runtime_error(
							"Cannot create tuple space directory: " + serverConfig.tupleSpaceDir);
					}
				}

				// Sprawdź czy katalog na przestrzeń krotek jest katalogiem i czy są odpowiednie prawa.
				if (!common::files::isDir(serverConfig.tupleSpaceDir)
					|| access(serverConfig.tupleSpaceDir.c_str(), W_OK | R_OK))
				{
					throw std::runtime_error(
						"Cannot access tuple space directory: " + serverConfig.tupleSpaceDir);
				}

				// Przejście do trybu daemon przed otwarciem plików.
				if (vm["daemon"].as<bool>())
				{
					try
					{
						std::string logFile = serverConfig.fifoPath + ".log";
						daemonize::turn_into_daemon();
						daemonize::save_pid(pidFileName);
						cm::log::to_file(logFile);
						BOOST_LOG_SEV(logger, info)<< "Daemon mode.";
					}
					catch (const std::exception& e)
					{
						BOOST_LOG_SEV(logger, error)<< "Critical error occurred while turning into daemon.";
						cm::print_exception(logger, e);
						return EXIT_FAILURE;
					}
				}
				else
				{
					BOOST_LOG_SEV(logger, info) << "Server is starting.";
				}

				Server server(serverConfig);
				// Konstruktor się powiódł zatem utworzył fifo
				// i poprawnie je otworzył. Można ustawić usuwanie fifo serwera.
				std::atexit(delete_server_fifo);

				server.run();
			}
			catch (const std::exception& e)
			{
				BOOST_LOG_SEV(logger, error)<< "Error occurred. Application will now exit.";
				cm::print_exception(logger, e);
				return EXIT_FAILURE;
			}
		}
		return EXIT_SUCCESS;
	}
	catch (const po::error& e)
	{
		// Jeżeli to nastąpi to na pewno przed wejściem w stan daemon.
		std::cerr << "Program options error occurred: " << e.what() << ".\n";
		std::cerr << "Use --help option to see help.\n";
		return EXIT_FAILURE;
	}
	catch (const std::exception& e)
	{
		BOOST_LOG_SEV(logger, fatal)<< "Unhandled exception reached the top of main.";
		cm::print_exception(logger, e);
		BOOST_LOG_SEV(logger, fatal)<< "Application will now exit.";
		return EXIT_FAILURE;
	}
	catch (...)
	{
		BOOST_LOG_SEV(logger, fatal)<< "Unhandled exception of unknown type reached the top of main.";
		BOOST_LOG_SEV(logger, fatal)<< "Application will now exit.";
		return EXIT_FAILURE;
	}
}
