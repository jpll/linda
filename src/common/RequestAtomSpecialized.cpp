#include "RequestAtomSpecialized.hpp"

template<class T>
RequestAtomSpecialized<T>::RequestAtomSpecialized(
		const RequestRelation& _relation, const T _value)
	: RequestAtom(_relation), value(_value)
{
	if(relation.getRelation() == Relation::ANY)
		throw std::runtime_error("Bad relation.");
}

template<class T>
RequestAtomSpecialized<T>::RequestAtomSpecialized() : RequestAtom(Relation::ANY)
{
}

template<>
std::string RequestAtomSpecialized<int>::serialize() const
{
	std::string ret = std::string("integer:") + relation.serialize();

	if(relation.getRelation() != Relation::ANY)
		ret += std::to_string(value);

	return ret;
}

template<>
std::string RequestAtomSpecialized<float>::serialize() const
{
	std::string ret = std::string("float:") + relation.serialize();

	if(relation.getRelation() != Relation::ANY)
		ret += std::to_string(value);

	return ret;
}

template<>
std::string RequestAtomSpecialized<std::string>::serialize() const
{
	std::string ret = std::string("string:") + relation.serialize();

	if(relation.getRelation() != Relation::ANY)
		ret += std::string("\"") + value + '"';

	return ret;
}


template<class T>
bool RequestAtomSpecialized<T>::match(const Atom* _at) const
{
	bool ret = false;

	if(relation.getRelation() == Relation::ANY && dynamic_cast<const AtomSpecialized<T>*>(_at) != nullptr)
		ret = true;
	else if(relation.getRelation() == Relation::EQ)
		ret = AtomSpecialized<T>(value) == _at;
	else if(relation.getRelation() == Relation::GR)
		ret = AtomSpecialized<T>(value) < _at;
	else if(relation.getRelation() == Relation::GRE)
		ret = AtomSpecialized<T>(value) <= _at;
	else if(relation.getRelation() == Relation::LW)
		ret = AtomSpecialized<T>(value) > _at;
	else if(relation.getRelation() == Relation::LWE)
		ret = AtomSpecialized<T>(value) >= _at;

	return ret;
}

template class RequestAtomSpecialized<int>;
template class RequestAtomSpecialized<float>;
template class RequestAtomSpecialized<std::string>;
