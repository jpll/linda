/*
 * common.hpp
 *
 *  Created on: 15 maj 2014
 *      Author: jakub
 */

#pragma once

#include <stdexcept>
#include <exception>

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>

#include <dirent.h>

#include <boost/shared_ptr.hpp>
#include <boost/log/core.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/record_ostream.hpp>

#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/utility/empty_deleter.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/lambda/lambda.hpp>

enum severity_level
{
	debug,
	info,
	warning,
	error,
	fatal
};

template<typename CharT, typename TraitsT>
inline std::basic_ostream<CharT, TraitsT>& operator<<(
		std::basic_ostream<CharT, TraitsT>& strm, severity_level lvl)
{
	static const char* const str[] =
			{
					"debug",
					"info",
					"warning",
					"error",
					"fatal"
			};
	if (static_cast<std::size_t>(lvl) < (sizeof(str) / sizeof(*str)))
		strm << str[lvl];
	else
		strm << static_cast<int>(lvl);
	return strm;
}

typedef boost::log::sources::severity_logger_mt<severity_level> Logger;

namespace common
{
	/**
	 * Prints the explanatory string of an exception. If the exception is nested,
	 * recurses to print the explanatory of the exception it holds.
	 * @param e
	 * @param level
	 */
	void print_exception(boost::log::sources::severity_logger_mt<severity_level> logger,
			const std::exception& e, int level = 0, severity_level severity_level = debug);

	namespace log
	{
		/**
		 *
		 */
		void to_terminal();

		/**
		 *
		 * @param fileName
		 */
		void to_file(std::string fileName);

		/**
		 * Ustawia severity_level.
		 * @param severity_level
		 */
		void set_severity_level(const severity_level severity_level);

		/**
		 * Wycisza wszystkie logi.
		 */
		void silent();

		/**
		 *
		 */
		inline void init()
		{
			boost::log::add_common_attributes();
			to_terminal();
		}

	} /* namespace common::log */

	namespace files
	{
		bool isDir(const std::string&);
	}

} /* namespace common */
