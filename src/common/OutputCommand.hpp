#ifndef OUTPUTCOMMAND_HPP_
#define OUTPUTCOMMAND_HPP_
#include <string.h>
#include "Command.hpp"
#include "Tuple.hpp"
#include "defines.hpp"

class OutputCommand: public Command
{
	Tuple tuple;
public:
	OutputCommand(Tuple);
	std::string serialize() const;
	bool execute(std::list<Tuple>& tupleSpace);
	std::chrono::milliseconds getTimeToDeadline();
};

#endif
