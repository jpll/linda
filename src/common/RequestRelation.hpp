#ifndef REQUESTRELATION_HPP_
#define REQUESTRELATION_HPP_
#include <string.h>
#include "Serializable.hpp"

enum Relation
{
	GR,		//greater
	GRE,	//greater or equal itd.
	EQ,
	LWE,
	LW,
	ANY
};

class RequestRelation : public Serializable
{

	Relation relation;
public :
	RequestRelation(const Relation);
	std::string serialize() const;

	Relation getRelation() const;
};

#endif
