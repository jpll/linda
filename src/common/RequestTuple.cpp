#include "RequestTuple.hpp"

RequestTuple::RequestTuple(const RequestTuple::RequestAtomList& requestAtoms)
	: requestAtoms(requestAtoms)
{
}

const RequestTuple::RequestAtomList& RequestTuple::getRequestAtomList() const
{
	return requestAtoms;
}

int RequestTuple::getLength() const
{
	return requestAtoms.size();
}

bool RequestTuple::match(const Tuple &tuple) const
{
	// Długości muszą się zgadzać.
	if (tuple.getLength() != getLength())
	{
		return false;
	}

	auto requestAtomIt = requestAtoms.begin();
	auto atomIt = tuple.getAtomList().begin();

	// Oraz wszystkie atomy pasować.
	while (requestAtomIt != requestAtoms.end())
	{
		if (!(*requestAtomIt)->match((*atomIt).get()))
		{
			return false;
		}
		++requestAtomIt;
		++atomIt;
	}

	return true;
}

std::string RequestTuple::serialize() const
{
	std::string tuple = "";

	for (const std::shared_ptr<const RequestAtom>& requestAtom : requestAtoms)
	{
		tuple += requestAtom->serialize() + ",";
	}

	if (tuple != "")
	{
		// Usuwamy ostatni przecinek.
		tuple = tuple.substr(0, tuple.size() - 1);
	}

	return "(" + tuple + ")";
}
