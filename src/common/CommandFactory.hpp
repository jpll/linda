#ifndef COMMANDFACTORY_HPP_
#define COMMANDFACTORY_HPP_

#include "OutputCommand.hpp"
#include "ReadCommand.hpp"
#include "InputCommand.hpp"

namespace CommandFactory
{
	std::unique_ptr<Command> createCommand(const std::string&);
}


#endif
