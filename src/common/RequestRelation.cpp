#include "RequestRelation.hpp"

RequestRelation::RequestRelation(const Relation _relation)
{
	relation = _relation;
}

std::string RequestRelation::serialize() const
{
	std::string ret;
	if(relation == Relation::GR)
		ret = ">";
	else if(relation == Relation::GRE)
		ret = ">=";
	else if(relation == Relation::EQ)
		ret = "==";
	else if(relation == Relation::LWE)
		ret = "<=";
	else if(relation == Relation::LW)
		ret = "<";
	else if(relation == Relation::ANY)
		ret = "*";

	return ret;
}

Relation RequestRelation::getRelation() const
{
	return relation;
}
