#pragma once

#include <memory>
#include "Atom.hpp"
#include "AtomSpecialized.hpp"
#include "RequestRelation.hpp"
#include "RequestAtom.hpp"
#include "RequestAtomSpecialized.hpp"

namespace AtomFactory
{
	std::unique_ptr<Atom> createAtom(const std::string&);
	Relation createRelation(const std::string&);
	std::unique_ptr<RequestAtom> createRequestAtom(const std::string&);
}
