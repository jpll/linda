#pragma once

#include <memory>
#include <list>

#include "RequestAtom.hpp"
#include "Tuple.hpp"

class RequestTuple : public Serializable
{
public:
	typedef std::list<std::shared_ptr<const RequestAtom>> RequestAtomList;
private:
	RequestAtomList requestAtoms;
public:
	RequestTuple(const RequestAtomList&);
	const RequestAtomList& getRequestAtomList() const;
	int getLength() const;
	bool match(const Tuple&) const;
	std::string serialize() const;
};
