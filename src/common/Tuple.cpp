#include "Tuple.hpp"

Tuple::Tuple(const Tuple::AtomList& atoms)
	: atoms(atoms)
{
}

const Tuple::AtomList& Tuple::getAtomList() const
{
	return atoms;
}

int Tuple::getLength() const
{
	return atoms.size();
}

std::string Tuple::serialize() const
{
	std::string tuple = "";

	for (const std::shared_ptr<const Atom>& atom : atoms)
	{
		tuple += atom->serialize() + ",";
	}

	if (tuple != "")
	{
		// Usuwamy ostatni przecinek.
		tuple = tuple.substr(0, tuple.size() - 1);
	}

	return "(" + tuple + ")";
}
