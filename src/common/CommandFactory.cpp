#include "CommandFactory.hpp"
#include "TupleFactory.hpp"
#include <stdexcept>

/*
 * Rzuca runtime_error w razie niepowodzenia
 */
std::unique_ptr<Command> CommandFactory::createCommand(const std::string& str)
{
	if(str.length() == 0)
		throw std::runtime_error("Empty string");

	std::unique_ptr<Command> command;

	if(str[0] == 'O')
	{
		std::unique_ptr<Tuple> tuple = TupleFactory::createTuple(str.substr(1, str.length() - 1));
		command = std::unique_ptr<Command>(new OutputCommand(*tuple));
	}
	else if(str[0] == 'I' || str[0] == 'R')
	{
		const int fifoEnd = str.find('(');
		std::string fifoName = str.substr(1, fifoEnd - 1);

		const int beforeDeadlineBeg = str.find_last_of(')'); //
		std::string deadlineString = str.substr(beforeDeadlineBeg + 1, str.length() - beforeDeadlineBeg -1);
		time_t deadline = std::stol(deadlineString);
		std::unique_ptr<RequestTuple> reqTuple = TupleFactory::createRequestTuple(str.substr(fifoEnd, beforeDeadlineBeg - fifoEnd + 1));
		if(str[0] == 'I')
			command = std::unique_ptr<Command>(new InputCommand(*reqTuple, fifoName, deadline));
		else if(str[0] == 'R')
			command = std::unique_ptr<Command>(new ReadCommand(*reqTuple, fifoName, deadline));
	}
	else
		throw std::runtime_error("Wrong string");

	return command;
}
