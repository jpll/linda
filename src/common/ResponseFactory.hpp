/*
 * ResponseFactory.hpp
 *
 *  Created on: 31 maj 2014
 *      Author: jakub
 */

#pragma once

#include "Response.hpp"
#include "ResponseDeadline.hpp"
#include "ResponseTuple.hpp"
#include "TupleFactory.hpp"

namespace ResponseFactory
{
	std::unique_ptr<Response> createResponse(std::string&);
} /* namespace ResponseFactory */
