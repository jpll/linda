#pragma once

#include "AtomFactory.hpp"
#include "RequestTuple.hpp"
#include "Tuple.hpp"

namespace TupleFactory
{
	std::unique_ptr<Tuple> createTuple(const std::string&);
	std::unique_ptr<RequestTuple> createRequestTuple(const std::string&);
}
