#ifndef REQUESTATOM_HPP_
#define REQUESTATOM_HPP_
#include "Serializable.hpp"
#include "RequestRelation.hpp"
#include "Atom.hpp"

class RequestAtom : public Serializable
{
protected:
	RequestRelation relation;
	RequestAtom(const RequestRelation&);
public:
	virtual bool match(const Atom*) const = 0;
};

#endif /* REQUESTATOM_HPP_ */
