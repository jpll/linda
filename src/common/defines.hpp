/*
 * defines.hpp
 *
 *  Created on: 15 maj 2014
 *      Author: jakub
 */

#pragma once

#define	EXIT_FAILURE	1	/* Failing exit status.  */
#define	EXIT_SUCCESS	0	/* Successful exit status.  */

#define BUF_SIZE 200  //maksymalny rozmiar krotki

// Jak będzie działać tworzenie folderów
#define LINDA_DIR "/tmp/linda/"

#define MAX_TIMEOUT 2147483647
