#pragma once

#include <string>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdexcept>

#include "../common/Command.hpp"
#include "../common/CommandFactory.hpp"
#include "../common/common.hpp"
#include "../common/defines.hpp"
#include "../common/ResponseFactory.hpp"

namespace linda
{
	struct LHandler
	{
			int clientFifoFd;
			int serverFifoFd;
			std::string tupleSpaceName;
			std::string clientFifoPath;
	}typedef LHandler;

	/**
	 * Przygotowuje FIFO klienta i serwera do użytku
	 * @param tupleSpaceName - nazwa przestrzeni krotek
	 */
	LHandler open(std::string tupleSpaceName);

	/**
	 * Umieszcza krotkę w przestrzeni
	 * @param tuple - krotka do umieszczenia w przestrzeni
	 * @param lhandler - deskryptory FIFO klienta i serwera
	 */
	void output(const Tuple& tuple, const LHandler& lhandler);

	/**
	 * Pobiera i usuwa z przestrzeni krotkę pasującą do wzorca
	 * @param reqTuple - krotka żądania
	 * @param deadline - czas do kiedy żądanie będzie ważne
	 * @param lhandler - deskryptory FIFO klienta i serwera
	 */
	std::unique_ptr<Response> input(const RequestTuple& reqTuple, time_t deadline, const LHandler& lhandler);

	/**
	 * Pobiera z przestrzeni krotkę pasującą do wzorca
	 * @param reqTuple - krotka żądania
	 * @param deadline - czas do kiedy żądanie będzie ważne
	 * @param lhandler - deskryptory FIFO klienta i serwera
	 */
	std::unique_ptr<Response> read(const RequestTuple& reqTuple, time_t deadline, const LHandler& lhandler);

	/**
	 * Usuwa FIFO klienta i serwera
	 * @param lhandler - deskryptory FIFO klienta i serwera oraz nazwa przestrzeni krotek
	 */
	void close(const LHandler& lhandler);
}
