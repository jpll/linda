#ifndef REQUESTINTEGER_HPP_
#define REQUESTINTEGER_HPP_
#include "RequestAtom.hpp"
#include "AtomSpecialized.hpp"
#include <stdexcept>

template<class T>
class RequestAtomSpecialized : public RequestAtom
{
	T value;
public :
	RequestAtomSpecialized(const RequestRelation&, const T);
	RequestAtomSpecialized();  //Relacja ANY

	std::string serialize() const;
	bool match(const Atom*) const;
};

#endif
