/*
 * ResponseTuple.cpp
 *
 *  Created on: 31 maj 2014
 *      Author: jakub
 */

#include "ResponseTuple.hpp"

ResponseTuple::ResponseTuple(const Tuple tuple):
	tuple(tuple)
{

}

ResponseTuple::~ResponseTuple()
{

}

std::string ResponseTuple::serialize()
{
	return "K" + this->tuple.serialize();
}

const Tuple& ResponseTuple::getTuple()
{
	return this->tuple;
}
