#ifndef INPUTCOMMAND_HPP_
#define INPUTCOMMAND_HPP_
#include <ctime>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "Command.hpp"
#include "RequestTuple.hpp"
#include "ResponseDeadline.hpp"
#include "ResponseTuple.hpp"

class InputCommand: public Command
{
	time_t deadline;
	std::string fifoResponse;
	RequestTuple requestTuple;

public:
	InputCommand(RequestTuple, std::string, time_t);
	std::string serialize() const;
	bool execute(std::list<Tuple>& tupleSpace);
	std::chrono::milliseconds getTimeToDeadline();

};

#endif
