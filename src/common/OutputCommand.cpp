#include "OutputCommand.hpp"

OutputCommand::OutputCommand(Tuple _tuple) :
		tuple(_tuple)
{
}

std::string OutputCommand::serialize() const
{
	return std::string("O") + tuple.serialize();
}

bool OutputCommand::execute(std::list<Tuple>& tupleSpace)
{
	tupleSpace.push_back(this->tuple);
	return true;
}

std::chrono::milliseconds OutputCommand::getTimeToDeadline()
{
	// Proste rozwiązanie.
	return std::chrono::milliseconds(MAX_TIMEOUT);
}
