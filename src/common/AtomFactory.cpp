#include "AtomFactory.hpp"

#include <stdexcept>

std::unique_ptr<Atom> AtomFactory::createAtom(const std::string& str)
{
	std::string floatBeg = "float:";
	std::string integerBeg = "integer:";
	std::string stringBeg = "string:";

	if (str.find(floatBeg) == 0)
	{
		std::string floatEnd = str.substr(floatBeg.size());
		// TODO: sprawdzić czy jest coś po liczbie?
		AtomSpecialized<float>* floatAtom = new AtomSpecialized<float>(std::stof(floatEnd));
		std::unique_ptr<Atom> floatAtomPtr(floatAtom);
		return floatAtomPtr;
	}
	else if (str.find(integerBeg) == 0)
	{
		std::string integerEnd = str.substr(integerBeg.size());
		// TODO: sprawdzić czy jest coś po liczbie?
		AtomSpecialized<int>* integerAtom = new AtomSpecialized<int>(std::stoi(integerEnd));
		std::unique_ptr<Atom> integerAtomPtr(integerAtom);
		return integerAtomPtr;
	}
	else if (str.find(stringBeg) == 0)
	{
		std::string stringEnd = str.substr(stringBeg.size());
		// Usunięcie "
		if (stringEnd.at(0) == '"' && stringEnd.at(stringEnd.size() - 1) == '"')
		{
			// -2 bo podaje się ile znaków ma być zwrócone począwszy od
			// pierwszego argumentu.
			std::string stringContent = stringEnd.substr(1, stringEnd.size() - 2);
			Atom* stringAtom = new AtomSpecialized<std::string>(stringContent);
			std::unique_ptr<Atom> stringAtomPtr(stringAtom);
			return stringAtomPtr;
		}
		throw std::runtime_error("Expected  \"");
	}
	throw std::runtime_error("Invalid atom prefix");
	return nullptr;
}

Relation AtomFactory::createRelation(const std::string& str)
{
	Relation relation;

	// Sprawdza tylko początek stringa, wtedy łatwiej jest użyć tej
	// funkcji w tworzeniu atomu zapytania.
	//
	// Teraz trzeba pamiętać jeszcze o dobrej kolejności sprawdzania.
	if(str.find(">=") == 0)
		relation = Relation::GRE;
	else if(str.find("==") == 0)
		relation = Relation::EQ;
	else if(str.find("<=") == 0)
		relation = Relation::LWE;
	else if(str.find(">") == 0)
		relation = Relation::GR;
	else if(str.find("<") == 0)
		relation = Relation::LW;
	else if(str.find("*") == 0)
		relation = Relation::ANY;
	else
		throw std::runtime_error("Invalid relation"); // TODO: lepszy wyjątek

	return relation;
}

std::unique_ptr<RequestAtom> AtomFactory::createRequestAtom(const std::string& str)
{
	std::string floatBeg = "float:";
	std::string integerBeg = "integer:";
	std::string stringBeg = "string:";

	if (str.find(floatBeg) == 0)
	{
		std::string floatRest = str.substr(floatBeg.size());
		RequestRelation relation = RequestRelation(createRelation(floatRest));
		if (relation.getRelation() == Relation::ANY)
		{
			RequestAtom* atom = new RequestAtomSpecialized<float>();
			std::unique_ptr<RequestAtom> ptr(atom);
			return ptr;
		}
		std::string floatEnd = floatRest.substr(relation.serialize().size());
		// TODO: sprawdzić czy jest coś po liczbie?
		float floatNumber = std::stof(floatEnd);
		RequestAtom* atom = new RequestAtomSpecialized<float>(relation, floatNumber);
		std::unique_ptr<RequestAtom> ptr(atom);
		return ptr;
	}
	else if (str.find(integerBeg) == 0)
	{
		std::string integerRest = str.substr(integerBeg.size());
		RequestRelation relation = RequestRelation(createRelation(integerRest));
		if (relation.getRelation() == Relation::ANY)
		{
			RequestAtom* atom = new RequestAtomSpecialized<int>();
			std::unique_ptr<RequestAtom> ptr(atom);
			return ptr;
		}
		std::string integerEnd = integerRest.substr(relation.serialize().size());
		int integerNumber = std::stoi(integerEnd);
		// TODO: sprawdzić czy jest coś po liczbie?
		RequestAtom* atom = new RequestAtomSpecialized<int>(relation, integerNumber);
		std::unique_ptr<RequestAtom> ptr(atom);
		return ptr;
	}
	else if (str.find(stringBeg) == 0)
	{
		std::string stringRest = str.substr(stringBeg.size());
		RequestRelation relation = RequestRelation(createRelation(stringRest));
		if (relation.getRelation() == Relation::ANY)
		{
			RequestAtom* atom = new RequestAtomSpecialized<std::string>();
			std::unique_ptr<RequestAtom> ptr(atom);
			return ptr;
		}
		std::string stringEnd = stringRest.substr(relation.serialize().size());
		// Usunięcie "
		if (stringEnd.at(0) == '"' && stringEnd.at(stringEnd.size() - 1) == '"')
		{
			// -2 bo podaje się ile znaków ma być zwrócone począwszy od
			// pierwszego argumentu.
			std::string stringContent = stringEnd.substr(1, stringEnd.size() - 2);
			RequestAtom* atom = new RequestAtomSpecialized<std::string>(relation, stringContent);
			std::unique_ptr<RequestAtom> ptr(atom);
			return ptr;
		}
		throw std::runtime_error("Expected  \"");
	}
	throw std::runtime_error("Invalid request atom prefix");
	return nullptr;
}
