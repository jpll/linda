#include "ReadCommand.hpp"

ReadCommand::ReadCommand(RequestTuple requestTuple, std::string fifoResponse,
		time_t deadline) :
		deadline(deadline), fifoResponse(fifoResponse), requestTuple(requestTuple)
{

}

std::string ReadCommand::serialize() const
{
	return std::string("R") + fifoResponse + requestTuple.serialize()
			+ std::to_string(deadline);
}

bool ReadCommand::execute(std::list<Tuple>& tupleSpace)
{
	std::list<Tuple>::const_iterator citer = tupleSpace.cbegin();
	std::list<Tuple>::const_iterator cend = tupleSpace.cend();

	int clientFifoFd;
	if ((clientFifoFd = open(fifoResponse.c_str(), O_WRONLY)) < 0)
	{
		// Jeżeli nie udało się otworzyć FIFO to nie przejmujemy
		// się dalej, bo i po co?
		return true;
	}

	while(citer != cend)
	{
		auto& tuple = *citer++;
		if(this->requestTuple.match(tuple))
		{
			ResponseTuple response(tuple);
			std::string msg = response.serialize() + "|";
			write(clientFifoFd, msg.c_str(), msg.length());
			return true;
		}
	}
	// Jeżeli nie minął termin wykonania
	// to wykonanie się nie powiodło
	if(getTimeToDeadline().count() > 0)
	{
		return false;
	}
	ResponseDeadline response;
	std::string msg = response.serialize() + "|";
	write(clientFifoFd, msg.c_str(), msg.length());
	return true;
}

std::chrono::milliseconds ReadCommand::getTimeToDeadline()
{
	// TODO: Zwiększyć rozdzielczość deadline
	return std::chrono::milliseconds((deadline - std::time(NULL)) * 1000);
}
