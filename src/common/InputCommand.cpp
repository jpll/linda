#include "InputCommand.hpp"

InputCommand::InputCommand(RequestTuple _requestTuple,
		std::string _fifoResponse, time_t _deadline) :
		deadline(_deadline), fifoResponse(_fifoResponse), requestTuple(
				_requestTuple)
{
}

std::string InputCommand::serialize() const
{
	return std::string("I") + fifoResponse + requestTuple.serialize()
			+ std::to_string(deadline);
}

bool InputCommand::execute(std::list<Tuple>& tupleSpace)
{
	std::list<Tuple>::iterator iter = tupleSpace.begin();
	std::list<Tuple>::iterator end = tupleSpace.end();

	int clientFifoFd;
	if ((clientFifoFd = open(fifoResponse.c_str(), O_WRONLY)) < 0)
	{
		// Jeżeli nie udało się otworzyć FIFO to nie przejmujemy
		// się dalej, bo i po co?
		return true;
	}

	while (iter != end)
	{
		auto& tuple = *iter;
		if (this->requestTuple.match(tuple))
		{
			ResponseTuple response(tuple);
			std::string msg = response.serialize() + "|";
			write(clientFifoFd, msg.c_str(), msg.length());
			tupleSpace.erase(iter);
			return true;
		}
		iter++;
	}
	// Jeżeli nie minął termin wykonania
	// to wykonanie się nie powiodło
	if (getTimeToDeadline().count() > 0)
	{
		return false;
	}
	ResponseDeadline response;
	std::string msg = response.serialize() + "|";
	write(clientFifoFd, msg.c_str(), msg.length());
	return true;
}

std::chrono::milliseconds InputCommand::getTimeToDeadline()
{
	// TODO: Zwiększyć rozdzielczość deadline
	return std::chrono::milliseconds((deadline - std::time(NULL)) * 1000);
}
