/*
 * ResponseDeadline.hpp
 *
 *  Created on: 31 maj 2014
 *      Author: jakub
 */

#pragma once
#include "Response.hpp"

class ResponseDeadline : public Response
{
	public:
		std::string serialize();
};

