#pragma once

#include <memory>
#include <list>

#include "Atom.hpp"

class Tuple : public Serializable
{
public:
	typedef std::list<std::shared_ptr<const Atom>> AtomList;
private:
	AtomList atoms;
public:
	Tuple(const AtomList&);
	const AtomList& getAtomList() const;
	int getLength() const;
	std::string serialize() const;
};
