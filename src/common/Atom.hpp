#ifndef ATOM_HPP_
#define ATOM_HPP_
#include <string>
#include "Serializable.hpp"

class Atom : public Serializable
{
public:
	virtual bool operator<(const Atom*) const = 0;
	virtual bool operator<=(const Atom*) const = 0;
	virtual bool operator==(const Atom*) const = 0;
	virtual bool operator>=(const Atom*) const = 0;
	virtual bool operator>(const Atom*) const = 0;

};



#endif
