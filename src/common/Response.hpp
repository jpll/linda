/*
 * Response.hpp
 *
 *  Created on: 31 maj 2014
 *      Author: jakub
 */

#pragma once

#include "Tuple.hpp"

class Response
{
	public:
		virtual ~Response() {};
		virtual std::string serialize() = 0;
};

