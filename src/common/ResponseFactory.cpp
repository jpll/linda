/*
 * ResponseFactory.cpp
 *
 *  Created on: 31 maj 2014
 *      Author: jakub
 */

#include "ResponseFactory.hpp"

#include <iostream>

namespace ResponseFactory
{
	std::unique_ptr<Response> createResponse(std::string& str)
	{
		std::unique_ptr<Response> response;
		if (str[0] == 'K')
		{
			auto tuple = TupleFactory::createTuple(str.substr(1));
			response = std::unique_ptr<Response>(new ResponseTuple(*tuple));
		}
		else if(str[0] == 'T')
		{
			response = std::unique_ptr<Response>(new ResponseDeadline);
		}
		else
		{
			throw std::runtime_error("Wrong string");
		}
		return response;
	}

} /* namespace ResponseFactory */
