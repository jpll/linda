/*
 * common.cpp
 *
 *  Created on: 15 maj 2014
 *      Author: jakub
 */

#include "common.hpp"

namespace common
{
	void print_exception(boost::log::sources::severity_logger_mt<severity_level> lg, const std::exception& e,
			int level, severity_level severity_level)
	{
		BOOST_LOG_SEV(lg, severity_level)<< std::string(level, ' ') << "Exception: " << e.what() << ".";
		try
		{
			std::rethrow_if_nested(e);
		}
		catch(const std::exception& e)
		{
			print_exception(lg, e, level+1, severity_level);
		}
		catch(...)
		{}
	}

	namespace log
	{
		namespace
		{
			severity_level global_severity_lvl = info;
		}

		void to_terminal()
		{
			boost::shared_ptr<boost::log::core> core = boost::log::core::get();
			core->remove_all_sinks();

			boost::shared_ptr<boost::log::sinks::basic_text_ostream_backend<char>> backendCout =
					boost::make_shared<boost::log::sinks::basic_text_ostream_backend<char>>();
			backendCout->add_stream(
					boost::shared_ptr<std::ostream>(&std::cout, boost::empty_deleter()));
			// Enable auto-flushing after each log record written
			backendCout->auto_flush(true);

			boost::shared_ptr<boost::log::sinks::basic_text_ostream_backend<char>> backendCerr =
					boost::make_shared<boost::log::sinks::basic_text_ostream_backend<char>>();
			backendCerr->add_stream(
					boost::shared_ptr<std::ostream>(&std::cerr, boost::empty_deleter()));
			// Enable auto-flushing after each log record written
			backendCerr->auto_flush(true);

			// Wrap it into the frontend and register in the core.
			// The backend requires synchronization in the frontend.
			typedef boost::log::sinks::synchronous_sink<boost::log::sinks::basic_text_ostream_backend<char>> sink_t;
			boost::shared_ptr<sink_t> sinkCout(new sink_t(backendCout));
			boost::shared_ptr<sink_t> sinkCerr(new sink_t(backendCerr));

			sinkCout->set_filter(boost::log::expressions::attr<severity_level>("Severity") < warning);
			sinkCerr->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= warning);

			sinkCout->set_formatter(
					boost::log::expressions::stream
							<< "[" << boost::log::expressions::attr<severity_level>("Severity")
							<< "] " << boost::log::expressions::smessage
							);

			sinkCerr->set_formatter(
					boost::log::expressions::stream
							<< "[" << boost::log::expressions::attr<severity_level>("Severity")
							<< "] " << boost::log::expressions::smessage
							);

			core->add_sink(sinkCout);
			core->add_sink(sinkCerr);

			core->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= global_severity_lvl);
		}

		void to_file(std::string fileName)
		{
			boost::shared_ptr<boost::log::core> core = boost::log::core::get();
			core->remove_all_sinks();

			boost::shared_ptr<boost::log::sinks::basic_text_ostream_backend<char>> backend =
					boost::make_shared<boost::log::sinks::basic_text_ostream_backend<char>>();

			backend->add_stream(
					boost::shared_ptr<std::ostream>(new std::ofstream(fileName)));
			// Enable auto-flushing after each log record written
			backend->auto_flush(true);

			typedef boost::log::sinks::synchronous_sink<boost::log::sinks::basic_text_ostream_backend<char>> sink_t;
			boost::shared_ptr<sink_t> sink(new sink_t(backend));

			sink->set_formatter(
					boost::log::expressions::stream
							<< std::hex << std::setw(8) << std::setfill('0')
							<< boost::log::expressions::attr<unsigned int>("LineID")
							<< ": ["
							<< boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp",
									"%Y-%m-%d %H:%M:%S")
							<< "] : [" << boost::log::expressions::attr<severity_level>("Severity")
							<< "] " << boost::log::expressions::smessage
							);

			core->add_sink(sink);

			core->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= global_severity_lvl);
		}

		void set_severity_level(const severity_level level)
		{
			global_severity_lvl = level;
			boost::log::core::get()->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= level);
		}

		void silent()
		{
			global_severity_lvl = error;
			boost::log::core::get()->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= error);
		}

	} /* namespace common::log */

	namespace files
	{
		bool isDir(const std::string& path)
		{
			DIR* dir = opendir(path.c_str());
			if (dir)
			{
				closedir(dir);
				return true;
			}
			return false;
		}
	}

} /* namespace common */
