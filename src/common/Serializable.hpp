#ifndef SERIALIZABLE_HPP_
#define SERIALIZABLE_HPP_

#include <string>

class Serializable
{
public:
	virtual std::string serialize() const= 0;
};


#endif /* SERIALIZABLE_HPP_ */
