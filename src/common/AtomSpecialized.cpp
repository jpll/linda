#include "AtomSpecialized.hpp"

template<class T>
AtomSpecialized<T>::AtomSpecialized(const T _value)
{
	value = _value;
}

template<class T>
T AtomSpecialized<T>::getValue() const
{
	return value;
}

template<class T>
bool AtomSpecialized<T>::operator<(const Atom* _at) const
{
	const AtomSpecialized<T>* specAt = dynamic_cast<const AtomSpecialized<T>*>(_at);

	if (specAt == nullptr)
		return false;
	else
		return value < specAt->value;
}

template<>
bool AtomSpecialized<std::string>::operator<(const Atom* _at) const
{
	const AtomSpecialized<std::string>* stringAt = dynamic_cast<const AtomSpecialized<std::string>*>(_at);

		if (stringAt == nullptr)
			return false;
		else
			return value.compare(stringAt->value) < 0;
}

template<class T>
bool AtomSpecialized<T>::operator<=(const Atom* _at) const
{
	const AtomSpecialized<T>* specAt = dynamic_cast<const AtomSpecialized<T>*>(_at);

	if (specAt == nullptr)
		return false;
	else
		return value <= specAt->value;
}

template<>
bool AtomSpecialized<std::string>::operator<=(const Atom* _at) const
{
	const AtomSpecialized<std::string>* stringAt = dynamic_cast<const AtomSpecialized<std::string>*>(_at);

		if (stringAt == nullptr)
			return false;
		else
			return value.compare(stringAt->value) <= 0;
}

template<>
bool AtomSpecialized<int>::operator==(const Atom* _at) const
{
	const AtomSpecialized<int>* intAt = dynamic_cast<const AtomSpecialized<int>*>(_at);

	if (intAt == nullptr)
		return false;
	else
		return value == intAt->value;
}

template<>
bool AtomSpecialized<float>::operator==(const Atom*) const
{
	return false;  //dla float ma nie byc okreslonego == więc zwracajac false nigdy się nie dopasuje
}

template<>
bool AtomSpecialized<std::string>::operator==(const Atom* _at) const
{
	const AtomSpecialized<std::string>* stringAt = dynamic_cast<const AtomSpecialized<std::string>*>(_at);

	if (stringAt == nullptr)
		return false;
	else
		return value.compare(stringAt->value) == 0;
}


template<class T>
bool AtomSpecialized<T>::operator>=(const Atom* _at) const
{
	const AtomSpecialized<T>* specAt = dynamic_cast<const AtomSpecialized<T>*>(_at);

	if (specAt == nullptr)
		return false;
	else
		return value >= specAt->value;
}

template<>
bool AtomSpecialized<std::string>::operator>=(const Atom* _at) const
{
	const AtomSpecialized<std::string>* stringAt = dynamic_cast<const AtomSpecialized<std::string>*>(_at);

	if (stringAt == nullptr)
		return false;
	else
		return value.compare(stringAt->value) >= 0;
}

template<class T>
bool AtomSpecialized<T>::operator>(const Atom* _at) const
{
	const AtomSpecialized<T>* specAt = dynamic_cast<const AtomSpecialized<T>*>(_at);

	if (specAt == nullptr)
		return false;
	else
		return value > specAt->value;
}

template<>
bool AtomSpecialized<std::string>::operator>(const Atom* _at) const
{
	const AtomSpecialized<std::string>* stringAt = dynamic_cast<const AtomSpecialized<std::string>*>(_at);

	if (stringAt == nullptr)
		return false;
	else
		return value.compare(stringAt->value) > 0;
}

template<>
std::string AtomSpecialized<int>::serialize() const
{
	return std::string("integer:") + std::to_string(value);
}

template<>
std::string AtomSpecialized<float>::serialize() const
{
	return std::string("float:") + std::to_string(value);
}

template<>
std::string AtomSpecialized<std::string>::serialize() const
{
	return std::string("string:") + '"' + value + '"';
}

template class AtomSpecialized<int>;
template class AtomSpecialized<float>;
template class AtomSpecialized<std::string>;
