#ifndef BLOCKINGQUEUE_HPP_
#define BLOCKINGQUEUE_HPP_
#include <queue>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <chrono>

template <class T>
class BlockingQueue
{
	std::queue<T> fifo;
	std::mutex mtx;
	std::condition_variable cond_empty;
	const unsigned int max_size;  //maksymalna ilość elementów w kolejce
public:
	BlockingQueue(const unsigned int _max_size = 2000) : max_size(_max_size) {}

	/**
	 * Zwraca true jeśli element wstawiono do kolejk, false w p.p.
	 */
	bool push(const T& element)
	{
		bool ret;

		//std::unique_lock<std::mutex> locker(mtx);
		mtx.lock();
		if(fifo.size() > max_size)
			ret = false;
		else
		{
			fifo.push(element);
			ret = true;
			cond_empty.notify_one();
		}
		mtx.unlock();
		return ret;
	}

	/*
	 * isSuccess wkazuje czy funkcja zwraca sensowną wartość, timeout domyślnie pół sekundy
	 */
	T offer(bool& isSuccess, const std::chrono::milliseconds& d = std::chrono::milliseconds(500))
	{
		std::unique_lock<std::mutex> locker(mtx);

		T element;
		std::cv_status status = std::cv_status::no_timeout;

		if (fifo.empty())
			status = cond_empty.wait_for(locker, d);

		if(status == std::cv_status::no_timeout && !fifo.empty())  //jeśli nie było timeout na cond_empty
		{
			element = fifo.front();
			fifo.pop();

			mtx.unlock();
			isSuccess = true;
		}
		else  //byl timeout na cond
		{
			isSuccess = false;
		}

		return element;
	}

	unsigned int size()
	{
		return fifo.size();
	}

};

#endif
