#include "TupleFactory.hpp"

#include <stdexcept>
#include <vector>
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>

std::unique_ptr<Tuple> TupleFactory::createTuple(const std::string& str)
{
	std::string tupleBeg = "(";
	std::string tupleSep = ",";
	std::string tupleEnd = ")";

	if (str.find(tupleBeg) == 0 && str.find(tupleEnd) == str.size() - 1)
	{
		std::list<std::shared_ptr<const Atom>> atoms;
		std::vector <std::string> vec;

		// Podział napisu bez nawiasów
		// TODO: uwzględnić przypadek napisu zawierającego przecinek
		boost::split_regex(vec, str.substr(1, str.size() - 2), boost::regex(tupleSep));
		for(auto elem : vec)
		{
		  atoms.push_back(AtomFactory::createAtom(elem));
		}

		Tuple* tuple = new Tuple(atoms);
		std::unique_ptr<Tuple> tuplePtr(tuple);
		return tuplePtr;
	}
	throw std::runtime_error("Expected  ( ... )");

	return nullptr;
}

std::unique_ptr<RequestTuple> TupleFactory::createRequestTuple(const std::string& str)
{
	std::string tupleBeg = "(";
	std::string tupleSep = ",";
	std::string tupleEnd = ")";

	if (str.find(tupleBeg) == 0 && str.find(tupleEnd) == str.size() - 1)
	{
		std::list<std::shared_ptr<const RequestAtom>> requestAtoms;
		std::vector <std::string> vec;

		// Podział napisu bez nawiasów
		// TODO: uwzględnić przypadek napisu zawierającego przecinek
		boost::split_regex(vec, str.substr(1, str.size() - 2), boost::regex(tupleSep));
		for(auto elem : vec)
		{
		  requestAtoms.push_back(AtomFactory::createRequestAtom(elem));
		}

		RequestTuple* requestTuple = new RequestTuple(requestAtoms);
		std::unique_ptr<RequestTuple> requestTuplePtr(requestTuple);
		return requestTuplePtr;
	}
	throw std::runtime_error("Expected  ( ... )");

	return nullptr;
}
