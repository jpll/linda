/*
 * ResponseTuple.hpp
 *
 *  Created on: 31 maj 2014
 *      Author: jakub
 */

#pragma once

#include "Response.hpp"
#include "Tuple.hpp"

class ResponseTuple: public Response
{
	const Tuple tuple;

	public:
		ResponseTuple(const Tuple);
		~ResponseTuple();
		std::string serialize();
		const Tuple& getTuple();
};

