#ifndef READCOMMAND_HPP_
#define READCOMMAND_HPP_
#include <ctime>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "Command.hpp"
#include "RequestTuple.hpp"
#include "ResponseDeadline.hpp"
#include "ResponseTuple.hpp"

class ReadCommand: public Command
{
	time_t deadline;
	std::string fifoResponse;
	RequestTuple requestTuple;
public:
	ReadCommand(RequestTuple, std::string, time_t);
	std::string serialize() const;
	bool execute(std::list<Tuple>& tupleSpace);
	std::chrono::milliseconds getTimeToDeadline();

};

#endif
