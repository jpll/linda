#ifndef COMMAND_HPP_
#define COMMAND_HPP_
#include "Serializable.hpp"
#include <chrono>
#include <list>
#include "Tuple.hpp"

/*
 * Klasa bazowa dla poleceń input, read, output
 */
class Command: public Serializable
{
	/*
	 * Pola:
	 * -deadline
	 * -tuple
	 * -requestTuple
	 * -fifoAns
	 * będą w klasach pochodnych bo żadne pole nie będzie we wszystkich.
	 */
public:
	/*
	 * Zwraca true jeśli polecenie udało się wykonać.
	 */
	virtual bool execute(std::list<Tuple>& tupleSpace) = 0;
	/**
	 * @return Czas do upłynięcia deadline w ms.
	 */
	virtual std::chrono::milliseconds getTimeToDeadline() = 0;
};


#endif
