#include "linda.hpp"

namespace linda
{
	void sendCommand(Command* command, int serverFifoFd)
	{
		std::string commandSerialized = command->serialize();
		if(commandSerialized.length() > 300)
		{
			throw std::logic_error("Command too long");
		}
		std::string message = commandSerialized + "|";
		if (::write(serverFifoFd, message.c_str(), message.size()) < 0)
		{
			throw std::runtime_error("Error while sending command");
		}
	}

	std::string getResponse(int clientFifoFd)
	{
		// Cały komunikat zwrotny będzie miał max 300 znaków
		// Krotka na pewno ma mniej niż 300 znaków
		// + 1 znak na oznaczenie komunikatu K lub T
		// + 1 znak na oddzielający '|'
		const int bufferSize = 350;
		char buffer[bufferSize];
		// W fifo powinna czekać jedna krotka
		int n;
		if ((n = ::read(clientFifoFd, buffer, bufferSize - 1)) > 0)
		{
			// Usuwam '|' z końca
			buffer[n - 1] = '\0';
			std::string bufferStr = std::string(buffer);
			return bufferStr;
		}
		throw std::runtime_error("Error while waiting for response");
	}

	LHandler open(std::string tupleSpaceName)
	{
		LHandler lhandler;

		lhandler.tupleSpaceName = tupleSpaceName;
		// Przygotowanie ścieżek do FIFO
		std::string tupleSpaceDir = std::string(LINDA_DIR) + tupleSpaceName + "/";
		lhandler.clientFifoPath = tupleSpaceDir + std::to_string(getpid());
		std::string serverFifoPath = tupleSpaceDir + tupleSpaceName;

		// Komunikat max 300 znaków
		// Jeden znak na oznaczenie komunikatu I|O|R
		// Do 79 znaków na ścieżkę FIFO
		// Do 20 znaków na deadline (obecnie zajmuje 10 znaków)
		// Co najmniej 200 znaków jest przeznaczonych na krotkę
		if(lhandler.clientFifoPath.length() > 79)
		{
			throw std::logic_error("Tuple space name too long");
		}

		// Utworzenie FIFO klienta
		const char* cFifoPath = lhandler.clientFifoPath.c_str();
		if (mkfifo(cFifoPath, S_IRUSR | S_IWUSR | S_IWGRP | S_IWOTH) < 0)
		{
			//Fifo file exists. It will be truncated and reused
		}

		// Otwieranie FIFO serwera
		if ((lhandler.serverFifoFd = ::open(serverFifoPath.c_str(), O_WRONLY)) < 0)
		{
			throw std::runtime_error("Cannot connect to server");
		}

		// Otwieranie FIFO klienta
		if ((lhandler.clientFifoFd = ::open(lhandler.clientFifoPath.c_str(), O_RDWR)) < 0)
		{
			throw std::runtime_error("Cannot open connection");
		}

		// Czyścimy FIFO, gdyby zostało użyte istniejące.
		::ftruncate(lhandler.clientFifoFd, 0);

		return lhandler;
	}

	void output(const Tuple& tuple, const LHandler& lhandler)
	{
		OutputCommand command(tuple);
		sendCommand(dynamic_cast<Command*>(&command), lhandler.serverFifoFd);
	}

	std::unique_ptr<Response> input(const RequestTuple& reqTuple, time_t deadline, const LHandler& lhandler)
	{
		InputCommand command(reqTuple, lhandler.clientFifoPath, deadline);
		sendCommand(dynamic_cast<Command*>(&command), lhandler.serverFifoFd);

		std::string bufferStr = getResponse(lhandler.clientFifoFd);
		return ResponseFactory::createResponse(bufferStr);
	}

	std::unique_ptr<Response> read(const RequestTuple& reqTuple, time_t deadline, const LHandler& lhandler)
	{
		ReadCommand command(reqTuple, lhandler.clientFifoPath, deadline);
		sendCommand(dynamic_cast<Command*>(&command), lhandler.serverFifoFd);

		std::string bufferStr = getResponse(lhandler.clientFifoFd);
		return ResponseFactory::createResponse(bufferStr);
	}

	void close(const LHandler& lhandler)
	{
		// Zamykanie FIFO klienta i serwera
		::close(lhandler.clientFifoFd);
		::close(lhandler.serverFifoFd);

		// Usuwanie FIFO klienta
		if (unlink(lhandler.clientFifoPath.c_str()) < 0)
		{
			throw std::runtime_error("Error while closing connection");
		}
	}
}
