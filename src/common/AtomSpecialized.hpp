#ifndef ATOMSPECIALIZED_HPP_
#define ATOMSPECIALIZED_HPP_
#include "Atom.hpp"

template<class T>
class AtomSpecialized : public Atom
{
	T value;
public:
	AtomSpecialized(const T);
	T getValue() const;
	bool operator<(const Atom*) const;
	bool operator<=(const Atom*) const;
	bool operator==(const Atom*) const;
	bool operator>=(const Atom*) const;
	bool operator>(const Atom*) const;
	std::string serialize() const;
};

#endif /* ATOMSPECIALIZED_HPP_ */
