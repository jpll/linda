#pragma once

#include <iostream>
#include <memory>
#include <mutex>
#include <thread>
#include <string>
#include <unistd.h>				// sleep
#include <fcntl.h>				// open
#include <sys/types.h>
#include <sys/stat.h>
#include <ctime>

#include "../common/Command.hpp"
#include "../common/CommandFactory.hpp"
#include "../common/common.hpp"
#include "../common/defines.hpp"
#include "../common/ResponseFactory.hpp"
#include "../common/linda.hpp"

class Client
{
public:
	enum class Operation { OUTPUT, INPUT, READ, ERROR };

	private:

		const linda::LHandler& lhandler;
		mutable Logger logger;

		Client::Operation parseOperation(const std::string&) const;
		std::unique_ptr<Tuple> parseTuple(const std::string&) const;
		std::unique_ptr<RequestTuple> parseRequestTuple(const std::string&) const;
		time_t parseDeadline(const std::string&) const;
		std::unique_ptr<Command> parseInput(std::string) const;

	public:
		Client(const linda::LHandler&);
		void run() const;

};
