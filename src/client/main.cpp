#include <iostream>
#include <functional>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <csignal>

#include <boost/program_options.hpp>

#include "Client.hpp"
#include "../common/defines.hpp"
#include "../common/common.hpp"

namespace
{
	Logger logger;
	linda::LHandler lhandler;
}

void kill_handler(int signum)
{
	BOOST_LOG_SEV(logger, debug)<< "Caught signal: " << signum;
	if(signum != SIGINT)
	{
		BOOST_LOG_SEV(logger, fatal) << "Error occurred.";
	}
	BOOST_LOG_SEV(logger, info) << "Exit.";
	exit(signum);
}

inline void install_signal_handlers()
{
	signal(SIGPIPE, kill_handler);
	signal(SIGINT, kill_handler);
	signal(SIGSEGV, kill_handler);
	signal(SIGTERM, kill_handler);
	signal(SIGABRT, kill_handler);
}

void delete_client_fifo()
{
	linda::close(lhandler);
	BOOST_LOG_SEV(logger, debug)<< "Fifo file deleted.";
}

int main(int argc, char **argv)
{
	namespace cm = common;
	namespace po = boost::program_options;

	try
	{
		install_signal_handlers();
		cm::log::init();

		po::options_description general("General options");
		general.add_options()
		("help,h", "Produce help message.")
		("tuple-space,t", po::value<std::string>()->required()->value_name("space"), "Tuple space.")
				;

		po::options_description special("Special options");
		special.add_options()
		("silent,S", po::bool_switch()->default_value(false), "Silent mode.")
		("debug,D", po::bool_switch()->default_value(false), "Print debug logs.")
				;

		po::options_description all("Allowed options");
		all.add(general).add(special);

		po::positional_options_description p;
		p.add("tuple-space", -1);

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				options(all).positional(p).run(), vm);

		if (vm.count("help"))
		{
			std::string usage(
					"Usage:"
					"\t client tuple-space-name [--debug | --silent]\n"
					);
			std::cout << usage << "\n";
			std::cout << all << "\n";
			return EXIT_SUCCESS;
		}

		// NOTIFY - dopiero tutaj sprawdzana program rzuca wyjątki
		// jeżeli coś jest nie tak z opcjami uruchomienia.
		po::notify(vm);

		if (vm["debug"].as<bool>())
		{
			cm::log::set_severity_level(debug);
		}

		if (vm["silent"].as<bool>())
		{
			// SILENT LOGS
			cm::log::silent();
		}

		if (vm.count("tuple-space"))
		{
			try
			{
				std::string tupleSpaceName = vm["tuple-space"].as<std::string>();

				// TODO: myślę, że warto dodać jeszcze jedną opcję
				// umożliwiająca zmianę domyślnego katalogu.

				lhandler = linda::open(tupleSpaceName);
				Client client(lhandler);
				// Konstruktor się powiódł zatem utworzył fifo
				// i poprawnie je otworzył. Można ustawić usuwanie fifo klienta.
				std::atexit(delete_client_fifo);

				client.run();
			}
			catch (const std::exception& e)
			{
				BOOST_LOG_SEV(logger, error)<< "Error occurred. Application will now exit.";
				cm::print_exception(logger, e);
				return EXIT_FAILURE;
			}
		}
	}
	catch (const po::error& e)
	{
		// Jeżeli to nastąpi to na pewno przed wejściem w stan daemon.
		std::cerr << "Program options error occurred: " << e.what() << ".\n";
		std::cerr << "Use --help option to see help.\n";
		return EXIT_FAILURE;
	}
	catch (const std::exception& e)
	{
		BOOST_LOG_SEV(logger, fatal)<< "Unhandled exception reached the top of main.";
		cm::print_exception(logger, e);
		BOOST_LOG_SEV(logger, fatal)<< "Application will now exit.";
		return EXIT_FAILURE;
	}
	catch (...)
	{
		BOOST_LOG_SEV(logger, fatal)<< "Unhandled exception of unknown type reached the top of main.";
		BOOST_LOG_SEV(logger, fatal)<< "Application will now exit.";
		return EXIT_FAILURE;
	}
}
