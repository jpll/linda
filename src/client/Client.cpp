#include "Client.hpp"

Client::Client(const linda::LHandler& lhandler)
	: lhandler(lhandler)
{
}

void Client::run() const
{
	BOOST_LOG_SEV(logger, info)<< "Client started.";

	// TODO: możliwość zatrzymania (aby wykonać polecenia z pliku).
	while (true)
	{
		std::cout << "> ";
		std::string input;
		std::getline(std::cin, input);
		if (std::cin.bad() || std::cin.fail() || std::cin.eof())
		{
			break;
		}
		try
		{
			// 201, bo jeden na O|I|R
			if (input.length() > 201)
			{
				BOOST_LOG_SEV(logger, info)<< "Too long input, try again";
				continue;
			}

			Operation op = parseOperation(input);
			if (op == Operation::OUTPUT)
			{
				std::unique_ptr<Tuple> tuple = parseTuple(input);
				// Wykonanie operacji output.
				linda::output(*tuple, lhandler);
				continue;
			}

			// Operacja input lub read, więc czytamy RequestTuple i deadline.
			std::unique_ptr<RequestTuple> reqTuple = parseRequestTuple(input);
			time_t deadline = parseDeadline(input);

			// Wysyłamy i czekamy na odpowiedź.
			std::unique_ptr<Response> response;
			if (op == Operation::INPUT)
			{
				response = linda::input(*reqTuple, deadline, lhandler);
			}
			else
			{
				response = linda::read(*reqTuple, deadline, lhandler);
			}

			// Prezentacja odpowiedzi.
			ResponseTuple* responseTuple = dynamic_cast<ResponseTuple*>(response.get());
			if (responseTuple != nullptr)
			{
				std::cout << responseTuple->getTuple().serialize() << "\n";
			}
			else
			{
				std::cout << "Deadline passed.\n";
			}
		}
		catch (const std::exception& e)
		{
			BOOST_LOG_SEV(logger, debug)<< e.what();
			std::cout << "Invalid command" << std::endl;
		}
	}
}

Client::Operation Client::parseOperation(const std::string& input) const
{
	switch (input.at(0))
	{
	case 'O':
		return Client::Operation::OUTPUT;
	case 'I':
		return Client::Operation::INPUT;
	case 'R':
		return Client::Operation::READ;
	}
	throw std::logic_error("Unknown command type");
	return Operation::ERROR;
}

std::unique_ptr<Tuple> Client::parseTuple(const std::string& input) const
{
	return TupleFactory::createTuple(input.substr(1));
}

std::unique_ptr<RequestTuple> Client::parseRequestTuple(const std::string& input) const
{
	std::string tupleOptionalDeadline = input.substr(1);
	std::string tupleNoDeadline =
		tupleOptionalDeadline.substr(0, tupleOptionalDeadline.find_last_of(")") + 1);
	return TupleFactory::createRequestTuple(tupleNoDeadline);
}

time_t Client::parseDeadline(const std::string& input) const
{
	size_t deadlinePos = input.find_last_of(")");
	if (deadlinePos == input.size() - 1)
	{
		// Nie ma deadline ustawionego przez użytkownika, ustaw długi domyślny.
		return time(NULL) + 10000;
	}
	else
	{
		std::string deadlineString = input.substr(deadlinePos + 1);
		return time(NULL) + std::stol(deadlineString);
	}
}
