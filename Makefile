CC := g++
SRCDIR := src
TESTDIR := test
BUILDDIR := build
BINDIR := bin
SERVER_TARGET := $(BINDIR)/server
SERVER_OBJECT := $(BUILDDIR)/server/main.o
CLIENT_TARGET := $(BINDIR)/client
CLIENT_OBJECT := $(BUILDDIR)/client/main.o
TEST_TARGET := $(BINDIR)/tester
TEST_MAIN := $(TESTDIR)/tester.cpp

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
TEST_SOURCES := $(shell find $(TESTDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
OBJECTS_NO_MAIN := $(filter-out $(SERVER_OBJECT) $(CLIENT_OBJECT),$(OBJECTS))
CFLAGS := -std=c++11 -g -Wall -Wextra -DBOOST_LOG_DYN_LINK
LIB := -pthread -lboost_unit_test_framework -lboost_log -lboost_log_setup -lboost_thread -lboost_system -lboost_program_options -lboost_regex
INC := $(foreach path,$(SRCDIR),-I $(path))

all: $(SERVER_TARGET) $(CLIENT_TARGET)

$(SERVER_TARGET): $(OBJECTS_NO_MAIN) $(SERVER_OBJECT)
	@mkdir -p $(BINDIR)
	$(CC) $^ -o $(SERVER_TARGET) $(LIB)

$(CLIENT_TARGET): $(OBJECTS_NO_MAIN) $(CLIENT_OBJECT)
	@mkdir -p $(BINDIR)
	$(CC) $^ -o $(CLIENT_TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

tester: $(TEST_TARGET)

$(TEST_TARGET): $(OBJECTS_NO_MAIN) $(TEST_MAIN) $(TEST_SOURCES)
	@mkdir -p $(BINDIR)
	$(CC) $(CFLAGS) $(INC) $^ -o $(TEST_TARGET) $(LIB)
	$(TEST_TARGET)

clean:
	$(RM) -r $(BUILDDIR) $(BINDIR)

.PHONY: all clean
