#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "common/Tuple.hpp"
#include "common/AtomSpecialized.hpp"

BOOST_AUTO_TEST_SUITE(TupleSuite)

BOOST_AUTO_TEST_CASE(testTuple)
{
	Tuple::AtomList atoms = {
		std::unique_ptr<Atom>(new AtomSpecialized<int>(1)),
		std::unique_ptr<Atom>(new AtomSpecialized<std::string>("str")),
    };

	Tuple tuple(atoms);

    BOOST_CHECK(tuple.getLength() == 2);
    BOOST_CHECK(tuple.getAtomList() == atoms);
    BOOST_CHECK(tuple.serialize() == "(integer:1,string:\"str\")");
}

BOOST_AUTO_TEST_SUITE_END();
