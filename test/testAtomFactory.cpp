#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "common/AtomFactory.hpp"

BOOST_AUTO_TEST_SUITE(AtomFactorySuite)

BOOST_AUTO_TEST_CASE(testCreatingFloatAtom)
{
    std::unique_ptr<Atom> atom = AtomFactory::createAtom("float:1.5");
    AtomSpecialized<float>* floatAtom = dynamic_cast<AtomSpecialized<float>*>(atom.get());
    BOOST_CHECK(floatAtom->getValue() == 1.5);
}

BOOST_AUTO_TEST_CASE(testCreatingIntegerAtom)
{
    std::unique_ptr<Atom> atom = AtomFactory::createAtom("integer:1.5");
    AtomSpecialized<int>* integerAtom = dynamic_cast<AtomSpecialized<int>*>(atom.get());
    BOOST_CHECK(integerAtom->getValue() == 1);
}

BOOST_AUTO_TEST_CASE(testCreatingStringAtom)
{
    std::unique_ptr<Atom> atom = AtomFactory::createAtom("string:\"str\"");
    AtomSpecialized<std::string>* stringAtom = dynamic_cast<AtomSpecialized<std::string>*>(atom.get());
    BOOST_CHECK(stringAtom->getValue() == "str");
}

BOOST_AUTO_TEST_CASE(testCreatingRelation)
{
	Relation relation = AtomFactory::createRelation("<");
	BOOST_CHECK(relation == Relation::LW);
	// itd.
}

BOOST_AUTO_TEST_CASE(testCreatingStringRequestAtom)
{
	std::unique_ptr<RequestAtom> requestAtom
		= AtomFactory::createRequestAtom("string:>=\"a b c\"");

	// Nie zwalniam pamięci.
	BOOST_CHECK(requestAtom.get()->match(new AtomSpecialized<std::string>("a b c")));
	BOOST_CHECK(requestAtom.get()->match(new AtomSpecialized<std::string>("a b d")));
	BOOST_CHECK(!requestAtom.get()->match(new AtomSpecialized<std::string>("a a a")));
	BOOST_CHECK(!requestAtom.get()->match(new AtomSpecialized<int>(1)));
}
// itd., może jest dobrze.

BOOST_AUTO_TEST_SUITE_END()
