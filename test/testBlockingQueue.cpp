#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "common/BlockingQueue.hpp"

BOOST_AUTO_TEST_SUITE(BlockingQueueSuite)

BOOST_AUTO_TEST_CASE(testPushAndOfferSuccess)
{
	BlockingQueue<int> q;
	q.push(5);
	bool success;
	int v = q.offer(success);
	BOOST_CHECK(v == 5);
	BOOST_CHECK(success == true);
}

BOOST_AUTO_TEST_CASE(testOfferTimeout)
{
	BlockingQueue<int> q;
	bool success;
	q.offer(success);
	BOOST_CHECK(success == false);
}

BOOST_AUTO_TEST_SUITE_END()
