#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "common/RequestTuple.hpp"
#include "common/AtomSpecialized.hpp"
#include "common/RequestAtomSpecialized.hpp"

BOOST_AUTO_TEST_SUITE(RequestTupleSuite)

BOOST_AUTO_TEST_CASE(testRequestTuple)
{
	RequestTuple::RequestAtomList requestAtoms = {
		std::unique_ptr<RequestAtom>(new RequestAtomSpecialized<int>(Relation::GR, 1)),
		std::unique_ptr<RequestAtom>(new RequestAtomSpecialized<std::string>()),
	};

	RequestTuple requestTuple(requestAtoms);

	BOOST_CHECK(requestTuple.getLength() == 2);
	BOOST_CHECK(requestTuple.getRequestAtomList() == requestAtoms);
	BOOST_CHECK(requestTuple.serialize() == "(integer:>1,string:*)");
}

BOOST_AUTO_TEST_CASE(testMatch)
{
	RequestTuple::RequestAtomList requestAtoms = {
		std::unique_ptr<RequestAtom>(new RequestAtomSpecialized<int>(Relation::GR, 1)),
		std::unique_ptr<RequestAtom>(new RequestAtomSpecialized<std::string>()),
	};
	RequestTuple requestTuple(requestAtoms);

	Tuple::AtomList matchingAtoms = {
		std::unique_ptr<Atom>(new AtomSpecialized<int>(2)),
		std::unique_ptr<Atom>(new AtomSpecialized<std::string>("str")),
    };
	Tuple matchingTuple(matchingAtoms);

	Tuple::AtomList notMatchingAtoms = {
		std::unique_ptr<Atom>(new AtomSpecialized<int>(0)),
		std::unique_ptr<Atom>(new AtomSpecialized<std::string>("str")),
    };
	Tuple notMatchingTuple(notMatchingAtoms);

	Tuple::AtomList notMatchingLengthAtoms = {
		std::unique_ptr<Atom>(new AtomSpecialized<std::string>("str")),
    };
	Tuple notMatchingLengthTuple(notMatchingLengthAtoms);

	BOOST_CHECK(requestTuple.match(matchingTuple));
	BOOST_CHECK(!requestTuple.match(notMatchingTuple));
	BOOST_CHECK(!requestTuple.match(notMatchingLengthTuple));
}

BOOST_AUTO_TEST_SUITE_END();
