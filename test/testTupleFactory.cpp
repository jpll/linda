#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "common/TupleFactory.hpp"

BOOST_AUTO_TEST_SUITE(AtomFactorySuite)

// krotki:
// brak wystąpienia podziału
BOOST_AUTO_TEST_CASE(testCreatingFirstTuple)
{
    std::unique_ptr<Tuple> tuple = TupleFactory::createTuple("(float:3.14)");
    Tuple::AtomList atoms = {
		std::unique_ptr<Atom>(new AtomSpecialized<float>(3.14)),
    };

	Tuple control(atoms);
	BOOST_CHECK(tuple->serialize() == control.serialize());
}

BOOST_AUTO_TEST_CASE(testCreatingSecondTuple)
{
    std::unique_ptr<Tuple> tuple = TupleFactory::createTuple("(integer:1,string:\"str\")");
    Tuple::AtomList atoms = {
		std::unique_ptr<Atom>(new AtomSpecialized<int>(1)),
		std::unique_ptr<Atom>(new AtomSpecialized<std::string>("str")),
    };

	Tuple control(atoms);
	BOOST_CHECK(tuple->getLength() == control.getLength());
	BOOST_CHECK(tuple->serialize() == control.serialize());
}

//krotki-żądania:
BOOST_AUTO_TEST_CASE(testCreatingRequestTuple)
{
	std::unique_ptr<RequestTuple> requestTuple = TupleFactory::createRequestTuple("(integer:>1,string:*)");
	RequestTuple::RequestAtomList requestAtoms = {
		std::unique_ptr<RequestAtom>(new RequestAtomSpecialized<int>(Relation::GR, 1)),
		std::unique_ptr<RequestAtom>(new RequestAtomSpecialized<std::string>()),
	};

	RequestTuple control(requestAtoms);

	BOOST_CHECK(requestTuple->getLength() == control.getLength());
	BOOST_CHECK(requestTuple->serialize() == control.serialize());
}

BOOST_AUTO_TEST_SUITE_END()
