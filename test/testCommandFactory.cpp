#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "common/CommandFactory.hpp"

#include <iostream>

BOOST_AUTO_TEST_SUITE(CommandFactorySuite)

BOOST_AUTO_TEST_CASE(testCreatingReadTuple)
{
	std::unique_ptr<Command> command =
			CommandFactory::createCommand(
					"R/tmp/folder/serwer(integer:==5,string:*,float:<5.010000,string:>=\"napis\")256");

	std::string expectedOutput = "R/tmp/folder/serwer(integer:==5,string:*,float:<5.010000,string:>=\"napis\")256";
	BOOST_CHECK(expectedOutput.compare(command->serialize()) == 0);
}

BOOST_AUTO_TEST_CASE(testCreatingOutputTuple)
{
	std::string commandStr = "O(integer:1)";
	std::unique_ptr<Command> command
		= CommandFactory::createCommand(commandStr);

	BOOST_CHECK(command->serialize() == commandStr);
}

BOOST_AUTO_TEST_SUITE_END()
