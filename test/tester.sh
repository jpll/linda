#!/bin/bash

LINDAPATH='./../bin'

# Reset
Color_Off='\e[0m'       # Text Reset

# Regular Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Underline
UBlack='\e[4;30m'       # Black
URed='\e[4;31m'         # Red
UGreen='\e[4;32m'       # Green
UYellow='\e[4;33m'      # Yellow
UBlue='\e[4;34m'        # Blue
UPurple='\e[4;35m'      # Purple
UCyan='\e[4;36m'        # Cyan
UWhite='\e[4;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

# High Intensity
IBlack='\e[0;90m'       # Black
IRed='\e[0;91m'         # Red
IGreen='\e[0;92m'       # Green
IYellow='\e[0;93m'      # Yellow
IBlue='\e[0;94m'        # Blue
IPurple='\e[0;95m'      # Purple
ICyan='\e[0;96m'        # Cyan
IWhite='\e[0;97m'       # White

# Bold High Intensity
BIBlack='\e[1;90m'      # Black
BIRed='\e[1;91m'        # Red
BIGreen='\e[1;92m'      # Green
BIYellow='\e[1;93m'     # Yellow
BIBlue='\e[1;94m'       # Blue
BIPurple='\e[1;95m'     # Purple
BICyan='\e[1;96m'       # Cyan
BIWhite='\e[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\e[0;100m'   # Black
On_IRed='\e[0;101m'     # Red
On_IGreen='\e[0;102m'   # Green
On_IYellow='\e[0;103m'  # Yellow
On_IBlue='\e[0;104m'    # Blue
On_IPurple='\e[0;105m'  # Purple
On_ICyan='\e[0;106m'    # Cyan
On_IWhite='\e[0;107m'   # White

# No tuple space specified
echo -e $IYellow"Running server with no tuple space specified:"
echo -e $White"---------------------------------------------"$Color_Off
echo ""

${LINDAPATH}/server

echo ""

echo -e $IYellow"Running client with no tuple space specified:"
echo -e $White"---------------------------------------------"$Color_Off
echo ""

${LINDAPATH}/client

# Server with tuple space name that already exists
echo ""
echo -e $IYellow"Server with tuple space name that already exists:"
echo -e $White"-------------------------------------------------"$Color_Off
echo ""

$LINDAPATH/server -t ts0 &
FIRST=($!)
echo -e $IPurple"${FIRST}: "$Color_Off

sleep 1

echo ""
$LINDAPATH/server -t ts0 &
SECOND=($!)
echo -e $IPurple"${SECOND}: "$Color_Off

sleep 1

echo ""
echo -e $Yellow"Closing ${FIRST}, ${SECOND} closed itself earlier"$Color_Off
kill ${FIRST}

sleep 1

# Trying to close not running daemon
echo ""
echo -e $IYellow"Close not running daemon:"
echo -e $White"-------------------------"$Color_Off
echo ""

$LINDAPATH/server -t ts0 &
SERVER=($!)
sleep 1

echo ""
echo -e $ICyan"(${LINDAPATH}/server -t ts0 -c)"$Color_Off
${LINDAPATH}/server -t ts0 -c

echo ""
echo -e $Yellow"Closing ${SERVER}"$Color_Off
kill ${SERVER}

sleep 1

# Closing server with running client
echo ""
echo -e $IYellow"Closing server with running client:"
echo -e $White"-----------------------------------"$Color_Off
echo ""

echo -e $ICyan"(${LINDAPATH}/server -t ts0 -d)"$Color_Off
${LINDAPATH}/server -t ts0 -d
sleep 2

echo 'I(integer:*,integer:*)' > tmp0
echo ""
echo -e $ICyan"(${LINDAPATH}/client -t ts0)"$Color_Off
${LINDAPATH}/client -D -t ts0 & < tmp0
rm tmp0

sleep 1

echo ""
echo -e $Yellow"Stopping daemon:"$Color_Off
${LINDAPATH}/server -t ts0 -c

# Checking tuple content validity
echo ""
echo -e $IYellow"Atom prefix validity:"
echo -e $White"----------------------"$Color_Off
echo ""

echo -e $ICyan"(${LINDAPATH}/server -t ts0 -d)"$Color_Off
${LINDAPATH}/server -t ts0 -d
sleep 2

echo -e $ICyan"(echo 'O(int:1)' > tmp0)"$Color_Off
echo 'O(int:1)' > tmp0
echo ""
echo -e $ICyan"(${LINDAPATH}/client -t ts0)"$Color_Off
${LINDAPATH}/client -D -t ts0 < tmp0
rm tmp0

sleep 1

echo ""
echo -e $IYellow"Relation validity:"
echo -e $White"-------------------"$Color_Off
echo ""

echo -e $ICyan"(echo 'O(integer:%2)' > tmp0)"$Color_Off
echo 'O(integer:%2)' > tmp0
echo ""
echo -e $ICyan"(${LINDAPATH}/client -t ts0)"$Color_Off
${LINDAPATH}/client -D -t ts0 < tmp0
rm tmp0

sleep 1

echo ""
echo -e $IYellow"Command type validity:"
echo -e $White"----------------------"$Color_Off
echo ""

echo -e $ICyan"(echo 'X(integer:2)' > tmp0)"$Color_Off
echo 'X(integer:2)' > tmp0
echo ""
echo -e $ICyan"(${LINDAPATH}/client -t ts0)"$Color_Off
${LINDAPATH}/client -D -t ts0 < tmp0
rm tmp0

sleep 1

echo ""
echo -e $IYellow"Too long command:"
echo -e $White"------------------"$Color_Off
echo ""

echo -e $ICyan"(echo 'O(integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2)' > tmp0)"$Color_Off
echo 'O(integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2,integer:2)' > tmp0
echo ""
echo -e $ICyan"(${LINDAPATH}/client -t ts0)"$Color_Off
${LINDAPATH}/client -D -t ts0 < tmp0
rm tmp0

sleep 1

echo ""
echo -e $IYellow"Too long tuple space name:"
echo -e $White"--------------------------"$Color_Off
echo ""

echo -e $ICyan"(echo 'O(integer:2)' > tmp0)"$Color_Off
echo 'O(integer:2)' > tmp0
echo ""
echo -e $ICyan"(${LINDAPATH}/server -t ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0 -d)"$Color_Off
${LINDAPATH}/server -t ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0 -d
echo -e $ICyan"(echo /tmp/linda/ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0/ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts
0ts0ts0ts0ts0ts0ts0ts0ts0.log)"$Color_Off
cat /tmp/linda/ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0/ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0ts0.log

# Cleanup

echo ""
echo -e $Yellow"Stopping daemon:"$Color_Off
${LINDAPATH}/server -t ts0 -c
