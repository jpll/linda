# Linda #

---

## System komunikacji między-procesowej z wykorzystaniem języka Linda z centralnym procesem koordynującym. ##

* Serwer zarządzający przestrzenią krotek. 
* API (linda.hpp)
* Komunikacja poprzez FIFO (potoki nazwane).
* Dostępny przykładowy program (client).

---

### Opis ###

W uproszczeniu język Linda realizuje trzy operacje:

	output(krotka)
	input(wzorzec-krotki) timeout [s]
	read(wzorzec-krotki) timeout [s]


Komunikacja między-procesowa w Lindzie realizowana jest poprzez wspólna dla wszystkich procesów przestrzeń krotek. Krotki są arbitralnymi tablicami dowolnej długości składającymi się z elementów 3 typów podstawowych: 

* string
* integer
* float.

Przykłady krotek:

1. (1, “abc”, 3.1415, “d”)
2. (10, “abc”, 3.1415)
3. (2, 3, 1, “Ala ma kota”). 

Działanie operacji:

* output umieszcza krotkę w przestrzeni. 
* input pobiera i atomowo usuwa krotkę z przestrzeni, przy czym wybór krotki następuje poprzez dopasowanie wzorca-krotki.
* read działa tak samo jak input lecz nie usuwa krotki z przestrzeni.

Wzorzec jest krotką, w której dowolne składniki mogą być niewyspecyfikowane “*” (podany jest tylko typ) lub zadanie warunkiem logicznym (==, <, <=, >, >=). Przykład input(integer:1, string:*, float:*, string:”d”) - pobierze pierwszą krotkę z przykładu wyżej zaś: input(integer:>0, string:”abc”, float:*) drugą.  Operacje read i input zawsze zwracają jedną krotkę. W przypadku gdy wyspecyfikowna krotka nie istnieje operacje read i input zawieszają się do czasu pojawienia się oczekiwanej danej, lub upłynięcia timeoutu.

---

### Licencja ###

[GNU GPL v3](http://www.gnu.org/licenses/gpl-3.0.txt)